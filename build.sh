#!/bin/bash

# build libtp3

case "$1" in
    clean)
        echo "Cleaning..."
        rm -rf release lib
        ;;

    debug)
        echo "Debug Building..."
        mkdir lib
        mkdir debug
        cd debug
        cmake -DCMAKE_BUILD_TYPE=Debug ..
        make clean
        make
        cp lib/libtp3.so ../lib
        ;;

    *)
        echo "Building..."
        mkdir lib
        mkdir release
        cd release
        cmake ..
        make clean
        make
        cp lib/libtp3.so ../lib
esac
exit 0
